<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Hive_HiveMerchant',
    __DIR__
);
