<?php

namespace Hive\HiveMerchant\Api;

/**
 *
*/ 
interface HiveDiscountInterface
{
   /**
    * @param string $usertoken
    * @param int|string $cartId
    * @param float|int|string $discount
    * @return boolean|string
    */
   public function applyDiscount($usertoken, $cartId, $discount);

   /**
    * @param  string $usertoken
    * @param  float $discount
    * @return boolean|string
    */
   public function guestCreateCoupon($usertoken, $discount);

   /**
    * @param string $usertoken
    * @return boolean|string
    */
   public function setUserToken($usertoken);

}
