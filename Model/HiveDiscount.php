<?php

namespace Hive\HiveMerchant\Model;

use Magento\Quote\Api\CartRepositoryInterface;
use Hive\HiveMerchant\Api\HiveDiscountInterface;
use \Magento\Core\Helper\Data;

class HiveDiscount implements HiveDiscountInterface
{

  /**
   * Quote / Cart Repository
   * @var CartRepositoryInterface $quoteRepository
   */
  protected $quoteRepository;

  protected $customerSession;
  protected $salesRule;
  protected $_dateFilter;
  protected $random;
  protected $httpClient;
  protected $storeManager;
  protected $catalogSession;

  protected $quoteManagement;

  private $siteUrl;
  private $hiveAPI;

  /**
   * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
   * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
   */
  public function __construct(
      \Magento\Customer\Model\Session $customerSession,
      \Magento\SalesRule\Model\Rule $salesRule,
      \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
      \Magento\Framework\Math\Random $random,
      \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
      \Magento\Framework\HTTP\ZendClient $httpClient,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Catalog\Model\Session $catalogSession,
      \Magento\Quote\Model\Quote $quoteManagement
  ) {
      $this->quoteRepository = $quoteRepository;
      $this->customerSession = $customerSession;
      $this->salesRule = $salesRule;
      $this->_dateFilter = $dateFilter;
      $this->random = $random;
      $this->httpClient = $httpClient;
      $this->storeManager = $storeManager;
      $this->siteUrl = $this->getBrandUrl();
      $this->hiveAPI = 'https://api.hivetogether.com';
      $this->catalogSession = $catalogSession;
      $this->quoteManagement = $quoteManagement;
  }

   /**
    * @param string $usertoken
    * @return boolean|string
    */
   public function setUserToken($usertoken){
    try {
      // validate token with Hive
      $this->catalogSession->setData('usertoken', $usertoken);
      return 'usertoken saved: ' . $usertoken;
    }
    catch(\Exception $e){
      return 'Error';
    }
   }

  /**
    * @param string $usertoken
    * @param int|string $cartId
    * @param float|int|string $discount
    * @return boolean|string
    */
  public function applyDiscount($usertoken, $cartId, $discount) {
    $paramUsertoken = $usertoken;
    // get usertoken from session
    $usertoken = $this->catalogSession->getData('usertoken');
    if (!$usertoken) {
      return 'not usertoken data';
    } elseif ($usertoken != $paramUsertoken) {
      return 'received usertoken is not the same as the saved into session';
    }
    try {
      $this->removeExpiredRules();
      // Get authorization from hive API
      // validate if user has enough credit
      $authorization = $this->getAuthorization($usertoken, $discount);

      if ($authorization['authorized']) {

        $couponCode = $this->createCartPriceRule($discount);
        // Apply coupon to cart
        if($this->applyCoupon($cartId, $couponCode)) {
          // Save applied coupon code into hive API
          $brandUrl = $this->getBrandUrl();

          $data = array(
            "usertoken" => $usertoken,
            "brand" => $brandUrl,
            "couponcode" => $couponCode,
            "credits" => $discount
          );

          $url = $this->hiveAPI . '/magento/coupon/apply';

          $client = new \Zend_Http_Client($url);
          $client->setHeaders('Content-type','application/json');
          $client->setParameterPost($data);
          $json = $client->request(\Zend_Http_Client::POST);
          $json = $json->getRawBody();
          $json = json_decode($json, true);
        }
        return  $couponCode;
      } else {
        return "not authorized";
      }
    }
    catch(\Exception $e) {
      return $e->getMessage();
    }
  }

  /**
    * @param  string $usertoken
    * @param  float $discount
    * @return boolean|string
    */
   public function guestCreateCoupon($usertoken, $discount){
      $this->createCoupon($usertoken, $discount);
      $couponCode = $this->createCoupon($usertoken, $discount);
      if ($couponCode) {
        return $couponCode;
      } else {
        return 'user not authorized';
      }
   }

  /**
    * @param  string $usertoken
    * @param  float $discount
    * @return boolean|string
    */
   private function createCoupon($usertoken, $discount) {
     try {
      // Get authorization from hive API
      $authorization = $this->getAuthorization($usertoken, $discount);

      if ($authorization['authorized']) {
        // Create coupon
        $couponCode = $this->createCartPriceRule($discount);
        // Apply coupon to cart
        $response = $couponCode;

      } else {
        $response = false;
      }
      return  $response;
    }
    catch(\Exception $e) {
       return $e->getMessage();
    }
  }

  /**
   * get the user applied coupon code from Hive API
   * @param  string $usertoken [description]
   * @return string|boolean            the coupon code or false
   */
  private function getHiveUserAppliedCoupon($usertoken) {
    $data = array(
     'usertoken' => $usertoken,
     'brand' => $this->siteUrl
    );
    $url = $this->hiveAPI . '/magento/user/coupon/status';

    $client = new \Zend_Http_Client($url);
    $client->setHeaders('Content-type','application/json');
    $client->setParameterPost($data);
    $json = $client->request(\Zend_Http_Client::POST);
    $json = $json->getRawBody();
    $json = json_decode($json, true);
    return $json['coupon'];
  }


  /**
   * call Hive API to remove the coupon applied info
   * @param  string $usertoken [description]
   * @return boolean            [description]
   */
  private function removeHiveAppliedCoupon($usertoken) {
    $data = array(
     'usertoken' => $usertoken,
     'brand' => $this->siteUrl
    );
    $url = $this->hiveAPI . '/magentoapi/user/coupon/remove';

    $client = new \Zend_Http_Client($url);
    $client->setHeaders('Content-type','application/json');
    $client->setParameterPost($data);
    $json = $client->request(\Zend_Http_Client::POST);
    $json = $json->getRawBody();
    $json = json_decode($json, true);
    return $json['success'];
  }

  /**
   * Create a new Cart Price Rule
   * @param  float $discount
   * @return string         generated coupon code
   */
  private function createCartPriceRule($discount) {
    try {
      $model = $this->salesRule;
      $couponCode = 'Hive'.$this->random->getRandomString(16);
      $data = array(
        'name' => 'Hive Discount - ' . $couponCode,
        'coupon_code' => $couponCode,
        'uses_per_coupon' => 1,
        'uses_per_customer' => 1,
        'sort_order' => 0,
        'discount_amount' => $discount,
        'discount_qty' => 0,
        'discount_step' => null,
        'store_labels' => array('0' => null, '1' => null),
        'is_active' => 1,
        'use_auto_generation' => 0,
        'is_rss' => 0,
        'stop_rules_processing' => 0,
        'apply_to_shipping' => 0,
        'description' => '',
        'from_date' => null,
        'to_date' => null,
        'simple_action' => 'cart_fixed',
        'coupon_type' => 2,
        'website_ids' => array('0' => intval($this->storeManager->getStore()->getId())),
        'customer_group_ids' => $this->getCustomerGroups(),
        'rule' => array(
          'conditions' => array(
            '1' => array(
              'type' => 'Magento\SalesRule\Model\Rule\Condition\Combine',
              'aggregator' => 'all',
              'value' => 1,
              'new_child' => null
            )),
          'actions' => array(
            '1' => array(
              'type' => 'Magento\SalesRule\Model\Rule\Condition\Product\Combine',
              'aggregator' => 'all',
              'value' => 1,
              'new_child' => null
            ))
          ),
        'form_key' => 'BrefSu4RjXt2su0k');


      $inputFilter = new \Zend_Filter_Input(
          ['from_date' => $this->_dateFilter, 'to_date' => $this->_dateFilter],
          [],
          $data
      );
      $data = $inputFilter->getUnescaped();

      // id process

      $validateResult = $model->validateData(new \Magento\Framework\DataObject($data));

      if (isset(
          $data['simple_action']
      ) && $data['simple_action'] == 'by_percent' && isset(
          $data['discount_amount']
      )
      ) {
          $data['discount_amount'] = min(100, $data['discount_amount']);
      }
      if (isset($data['rule']['conditions'])) {
          $data['conditions'] = $data['rule']['conditions'];
      }
      if (isset($data['rule']['actions'])) {
          $data['actions'] = $data['rule']['actions'];
      }
      unset($data['rule']);
      $model->loadPost($data);

      $useAutoGeneration = (int)(
          !empty($data['use_auto_generation']) && $data['use_auto_generation'] !== 'false'
      );
      $model->setUseAutoGeneration($useAutoGeneration);

      $this->customerSession->setPageData($model->getData());

      $model->save();

      return $couponCode;
    } catch(\Exception $e) {

    }
  }

  /**
   * Apply coupon to cart
   * @param  int $cartId
   * @param  string $couponCode
   * @return boolean
   */
  private function applyCoupon($cartId, $couponCode) {
    $quote = $this->quoteRepository->getActive($cartId);
    if (!$quote->getItemsCount()) {
        throw new  \Magento\Framework\Exception\NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
    }
    $quote->getShippingAddress()->setCollectShippingRates(true);

    try {
        $oldCouponCode = $quote->getCouponCode();
        $quote->setCouponCode($couponCode);
        $this->quoteRepository->save($quote->collectTotals());
        if ($oldCouponCode && $oldCouponCode != '') {
          $this->deleteSalesRule($oldCouponCode);
        }
    } catch (\Exception $e) {
        throw new  \Magento\Framework\Exception\CouldNotSaveException(__('Could not apply coupon code'));
    }
    if ($quote->getCouponCode() != $couponCode) {
        throw new  \Magento\Framework\Exception\NoSuchEntityException(__('Coupon code is not valid'));
    }
    return true;
  }

  /**
   * Delete a sales rule
   * @param  string $couponCode
   * @return void
   */
  private function deleteSalesRule($couponCode) {
    $model = $this->salesRule
    ->getCollection()
    ->addFieldToFilter('name',array('eq'=> sprintf('Hive Discount - %s', $couponCode)))
    ->getFirstItem();

    $model->delete();
  }

  /**
   * Get authorization from Hive API to apply discount
   * @param  float $discount disccount to be applied
   * @return array           response from Hive API
   */
  private function getAuthorization($usertoken, $discount) {

    $data = array(
     'usertoken' => $usertoken,
     'brand' => $this->siteUrl,
     'credits' => $discount
    );

    // Hive backend is 127.0.0.1:3000 inside Vagrant
    $url = $this->hiveAPI . '/user/credit/apply/authorization';

    // Send request
    $client = new \Zend_Http_Client($url);
    $client->setHeaders('Content-type','application/json');
    $client->setParameterPost($data);
    $json = $client->request(\Zend_Http_Client::POST);
    $json = $json->getRawBody();
    $json = json_decode($json, true);
    // return response
    return $json;
  }

  /**
   * return customer groups array necessary to create a coupon
   * @return int[]
   */
  private function getCustomerGroups() {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();
    $customerGroups = [];
    for ($i=0; $i<count($groupOptions); $i++) {
      $customerGroups[$i] = intval($groupOptions[$i]['value']);
    }
    return $customerGroups;
  }

  /**
   * remove the rules that are already expired
   */
  private function removeExpiredRules() {
    try {
      $res = '';

      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $dateTime = $objectManager->get('\Magento\Framework\Stdlib\DateTime\DateTime');
      $date = $dateTime->date("Y-m-d");

      // find Hive rules that was created before today
      $model = $this->salesRule
      ->getCollection()
      ->addFieldToFilter('name',array('like'=> 'Hive Discount%'))
      ->addFieldToFilter('to_date', ['lt' => $date]);

      // Delete all the rules created before today
      foreach ($model as $value) {
         $res .= print_r($value->getData("name"),true) . " Deleted\n";
         // Delete hive rules that are expired
         $value->delete();
      }
    }
    catch (\Exception $e) {
      // Do something
      return $e;
    }

  }

   /**
   * Get the current brand url
   * @param  [type] $url [description]
   * @return [type]      [description]
   */
  private function getBrandUrl() {
    $url = $this->storeManager->getStore()->getBaseUrl();
    $brandUrl = str_replace('https://', '', $url);
    $brandUrl = str_replace('http://', '', $url);
    // remove the end /
    if (substr($brandUrl, -1) == '/') {
      $brandUrl = substr($brandUrl, 0, (strlen($brandUrl)-1));
    }
    return $brandUrl;
  }


}
