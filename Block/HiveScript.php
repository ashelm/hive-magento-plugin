<?php
namespace Hive\HiveMerchant\Block;

/**
 *
 */
class HiveScript extends \Magento\Framework\View\Element\Template
{
    /**
     *
     */
    public function getHiveScriptUrl()
    {
        $url = "https://hiveplugin.s3.amazonaws.com/magentoscript.js";
    	  //$url = "https://s3.amazonaws.com/testhiveplugin/magentoscript.js";
    	  //$url = 'http://127.0.0.1:8080/pub/static/frontend/Magento/blank/en_US/Hive_HiveMerchant/js/magentoscript.js';
        return $url;
    }
}
