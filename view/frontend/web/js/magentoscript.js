(function() {
/* global window, console, CrossStorageClient, localStorage, sessionStorage, setTimeout, attachEvent */
  var urlHost = window.location.host;
  //var urlProtocol = window.location.protocol;
  //var urlPath = window.location.pathname;
  var url = window.location.href;
  var baseUrl = window.location.protocol + '//' + window.location.host;
  var userToken = "";
  var app = null;


  var serverapi='http://localhost:8082';
  /** Detects if requirejs is present in the current webpage to load the required libraries */
  var hasRequireJs = (typeof require === "function" && typeof require.specified === "function");

  /* If jQuery UI has not yet been loaded or if it has but it's too old for our needs,
   we will load jQuery from the Google CDN, and when it's fully loaded, we will run
   our app's JavaScript. Set your own limits here, the sample's code below uses 1.7
   as the minimum version we are ready to use, and if the jQuery is older, we load 1.9. */
  var hasBootstrapSlider = (typeof(jQuery("body").slider) === 'function');

  /* Load Script function we may need to load jQuery from the Google's CDN */
  /* That code is world-reknown. */
  /* One source: http://snipplr.com/view/18756/loadscript/ */
  var loadScript = function (url, callback) {

      if (!hasRequireJs) {
        var script = document.createElement("script");
        script.type = "text/javascript";

        // If the browser is Internet Explorer.
        if (script.readyState) {
            script.onreadystatechange = function () {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
            // For any other browser.
        } else {
            script.onload = function () {
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
      } else {
        require([url], function() { callback(); });
      }
  };

  console.log( "Welcome to hive - Version 0.0.7");
  // Check if the local storage has some information about the Hive Session stored
  if( typeof localStorage.hivePlugin === "undefined"
   && typeof sessionStorage.hiveTokenSet === "undefined") {

      // Load the Cross-Storage library to get information
      var scriptUrl = serverapi;
      if (!hasRequireJs) {
        scriptUrl += '/client.min.js';
      } else {
        scriptUrl += '/scripts/client.no-require.js';
      }
      loadScript(scriptUrl, function() {
        try {
          var storage = new CrossStorageClient(serverapi+'/hub');
          storage.onConnect().then(function() {
              // Try to see if there is a session information stored
              return storage.get('userToken');
            }).then(function(token) {
              if (token) {
                sessionStorage.hiveTokenSet = true;
                sessionStorage.hivePlugin = token;
                localStorage.hivePlugin = token;
                userToken = token;
                loadApp();
              } else {
                console.log("Cross-storage token not found or was null. User will have to login to enter to Hive.");
                userToken = '';
                loadApp();
              }
          })['catch'](function(err) {
            console.log("Error on cross-storage query: " + err);
            userToken = '';
            loadApp();
          });
        } catch (error) {
          console.log("Could not load the CrossStorageClient: " + error);
          userToken = '';
          loadApp();
        }
      });
  } else {
      userToken = localStorage.hivePlugin;
      loadApp();
  }

  /* This is my app's JavaScript */
  var myAppJavaScript = function () {
    //Include styles for plugin loaded from AWS
    jQuery("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "https://s3.amazonaws.com/testhiveplugin/pluginstyles.css"
    }).appendTo("head");

    //Include styles for bootstrap slider available from CDN
    jQuery("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/6.0.16/css/bootstrap-slider.min.css"
    }).appendTo("head");

    var contenturl = "https://s3-us-west-2.amazonaws.com/hiveplugin/";
    var facebookclient = "573345552863225";
    var host = urlHost;
    //var isPluginSet = false;
    var brand_credits_applied = 0;
    var bodyDisplay = "none";

    var isBrandFavorite = false;
    var pluginwindow;
    var triedToLogin = false;

    // Display configuration
    var logoUrl = 'hivelogo.png';
    var bodycolor = '#56dbff';
    var textcolor = '#000000';
    var earningcolor = '#ffffff';
    var headerbuttoncolor =  "#0071bc";
    var submitbuttoncolor = '#ffdf7f';
    var pluginposition = 0;

    var earning_percent_initial = 0;
    var credits_applied = 0;

    var templates = Object.create(null);
    templates['cloudHive'] = '<div  id="hiveThinks" class="row" style="display: none;">\n    <div class="cloud_image" style="background-image: url(\'https://s3.amazonaws.com/testhiveplugin/img/hivethinks.png\');">\n      <div id="hiveText">{{:HiveThinks}}</div>\n    </div>\n</div>\n';
    templates['hPDisplay_cActive'] = '<div class=\'plugin-body\'>\n    <div class=\'plugin-brandname\'>{{:brandName}}</div>\n        <img src=\'{{:serverApi}}{{:brandImageLocation}}\' class=\'hive-brand-img-logo\' style=\'width:50%; margin-left:25%;\'/>\n    <div class=\'plugin-earning-head\'>\n        ${{:activeCredits}}\n    </div>\n</div>\n    <div class=\'plugin-body-3\'>\n        <div class=\'plugin-body-content-3\'>\n            <div>\n                <span class="earn-percent">Earning Percentage: </span>\n                <span class=\'percent-span earningstyle\' style=\'color:{{:earningColor}}\'> {{:earningPercent}}%</span>\n            </div>\n            <div class="credits-applied">\n                <span class=\'credit-text\'>Credits Applied: </span>\n                <input class=\'credits-amount percent-span\' type=\'text\' id=\'hivecreditamount\' readonly style=\'color: {{:earningcolor}}\'>\n            </div>\n        </div>\n    </div>\n    <div class="slider-credits">\n        <div class=\'slider-box\'>\n            <div id=\'slider\' class=\'slider-apply\' style=\'width: 100%;\'>\n            </div>\n        </div>\n        <div class="text-field-credit" id="textFieldCredit" style=\'display:none\'>\n            <div class="apply-credit-text">\n                Apply Credit:\n            </div>\n            <div>\n                <label class="dollar-sign-text">$</label>\n                <input type="text" id=\'hiveCreditsInput\' class="apply-credit-input" placeholder="00.00" style="margin-bottom:4px;" >\n            </div>\n        </div>\n        <span class=\'hiveError\'></span>\n        <span class=\'hiveSuccess\'></span>\n        <div id=\'hiveApplyCredits\' class=\'btn submit-button submit-apply\' style=\'background-color:{{:submitButtonColor}}\'>\n            Apply\n        </div>\n        <span><img src=\'{{:contentUrl}}{{:fav}}\' class=\'pluginfavoriteimg favCreditsPresent\'/> {{:toEarn}}</span>\n    </div>\n';
    templates['hPDisplay_cHive'] = '    <div class=\'plugin-body\' style=\'width: 50%; margin-top:0%;\'>\n        <div class=\'plugin-brandname\'>\n            {{:brandName}}\n        </div>\n        <img src=\'{{:serverApi}}{{:brandImageLocation}}\' class=\'hive-brand-img-logo\'/>\n    </div>\n    <div class=\'plugin-hive-credit\'>\n        Friends shopping here:\n    </div>\n    <div class=\'plugin-body-2\' style=\'width: 50%;float: right;\'>\n        <div class=\'plugin-body-content\'>\n            {{:spanCredits}}\n            <div>\n                <span>Earning Percentage: </span>\n                <span class=\'percent-span earningstyle\' style=\'color:{{:earningColor}}\'> {{:earningPercent}}%</span>\n            </div>\n        </div>\n    </div>\n    <div class=\'shop-favorite\'>\n        <img src=\'{{:contentUrl}}{{:fav}}\' class=\'pluginfavoriteimg\'/> {{:toEarn}}\n    </div>\n';
    templates['hPDisplay_cZero'] = '<div class=\'plugin-body\' style=\'width: 50%; margin-top:0%;\'>\n    <div class=\'plugin-brandname\'>\n        {{:brandName}}\n    </div>\n    <img src=\'{{:serverApi}}{{:brandImageLocation}}\' class=\'hive-brand-img-logo\'/>\n</div>\n<div class=\'plugin-body-2\' style=\'width: 50%;float: right;margin-top: 3%;\'>\n    <div class=\'plugin-body-content\'>\n        <div class=\'left-block\'>\n            <span>If you make</span><br> \n            <span>a purchase from us,</span><br> \n            <span>you will earn</span><br> \n            <span class=\'percent-span earningstyle\' style=\'color: {{:earningColor}}\'>{{:earningPercent}}%</span><br> \n            <span>in store credit</span>\n        </div>\n        <span><img src=\'{{:contenturl}} {{:fav}}\' class=\'pluginfavoriteimg\'/>{{:toEarn}}</span>\n    </div>\n</div>';
    templates['hPDisplay_header'] = '<div class=\'plugin-label\'>\n    <div class=\'plugin-username\'>Hello {{:userName}}</div>\n</div>\n';
    templates['hPRegister'] = '<div id=\'hive-plugin-register-body\' class=\'hivedialog\'>\n    <div id=\'register\' class=\'hive-plugin-data\' style=\'background-color:{{:bodyColor}}; color:{{:textColor}}; display:{{:displaybody}}\'>\n    </div>\n</div>';
    templates['hiveLogo'] = '<img id=\'hive_display_logo\' src=\'{{:logoHiveUrl}}\' class=\'hive-plugin-logo\' style=\'display:none\'/>\n<div id=\'dialog\'></div>\n';
    templates['login'] = '<div class=\'hive-login-container\' style=\'color: {{:textColor}}\'>\n    <div class=\'hive-plugin-button-container\'>\n        <button class=\'btn loginButton\' style=\'background:{{:headerButtonColor}};color:{{:textColor}}\'> Log In </button>\n        <button class=\'btn registerButton\' style=\'background:{{:bodyColor}}; color:{{:textColor}}\'> Sign Up </button>\n    </div>\n    <div id="plugin-register" class=\'plugin-register-text-body\'>\n        <div class=\'plugin-register-intro\'>\n            <span>Sign up For</span>\n            <span> Hive and earn </span>\n            <span class=\'percent-span earningstyle\' style=\'color:{{:earningColor}}\'>{{:initialEarningPercent}}%</span>\n            <span>in Store Credit </span>\n            <span>on this purchase.</span>\n            <span class=\'site-url\'><a href="https://www.hivetogether.com">www.hivetogether.com</a></span>\n            <span class=\'plugin-error-message\'></span>\n        </div>\n        <div class=\'plugin-register-form\'>\n            <input type=\'text\' class=\'form-control hive-reg-username hive-plugin-text\' style=\'margin-bottom:4px;\' placeholder=\'Full Name\'/>\n            <input type=\'text\' class=\'form-control hive-reg-useremail hive-plugin-text\' style=\'margin-bottom:4px;\' placeholder=\'Email\'/>\n            <input type=\'password\' class=\'form-control hive-reg-userpassword hive-plugin-text\' style=\'margin-bottom:4px;\' placeholder=\'Password\'/>\n            <div class=\'\'> <input type=\'checkbox\' class=\'plugintnc\'> Accept<span class=\'accepttnc\'> T & C</span></input></div>\n                <div disabled class=\'reg-submit-btn\' style=\'background-color:{{:submitButtonColor}}\'>Sign Up</div>\n                <div class="fb-img-container" id="fb-button">\n                    <img src="https://s3.amazonaws.com/testhiveplugin/img/facebook_logo.png" class="fb_image" alt="facebook_login">\n                    <img src="https://s3.amazonaws.com/testhiveplugin/img/blue_facebook_logo.png" class="fb_image2" alt="facebook_login">\n                </div>\n            </div>\n    </div>\n    <div id="plugin-login" class=\'plugin-register-text-body\' style=\'display:none;\'>\n        <div class=\'plugin-register-intro\'>\n            <span>Login to</span>\n            <span> Hive and earn </span>\n            <span class=\'percent-span earningstyle\' style=\'color:{{:earningColor}}\'>{{:initialEarningPercent}}%</span>\n            <span>in Store Credit </span>\n            <span>on this purchase.</span>\n            <span class=\'site-url\'><a href=\'www.hivetogether.com\'>www.hivetogether.com</a></span>\n        </div>\n\n        <div class=\'plugin-register-form\'>\n            <input type=\'text\' class=\'form-control hive-login-useremail hive-plugin-text\' style=\'margin-bottom:4px;\' placeholder=\'Email\'/>\n            <input type=\'password\' class=\'form-control hive-login-userpassword hive-plugin-text\' style=\'margin-bottom:4px;\' placeholder=\'Password\'/>\n            <div class=\'login-submit-btn\' style=\'background-color:{{:submitButtonColor}}\'>Log In</div>\n            <span class=\'fb-img-container\'></span>\n            <div class=\'hive-plugin-error\'></div>\n        </div>\n\n    </div>\n</div>\n';
    templates['welcome'] = '<div class=\'plugin-label\'>\n      <div class=\'plugin-username\'>Hello {{:username}}</div>  \n  </div>\n';

    /**
     * Wrapper function to have working this on Magento version
     * on Shopify and Magento there is different versions of JQuery
     * @param name Name of the template
     * @returns object rendered object to be used
     */
    function getTemplate(name) {
      if (typeof(window.jsrender) !== 'undefined') {
        return window.jsrender.templates(name);
      } else if (typeof(jQuery.templates) === 'function') {
        return jQuery.templates(name);
      }
      console.log("Template generator not found");
      return  {
        render: function(temp) {
          console.log(temp);
        }
      };
    }  // End get template function

    /**
     * Function that stores the current elemet inside teh magento application
     * @param usertoken User token
     */
    function saveUserTokenToMagento (usertoken) {
        if(usertoken && usertoken !== '') {
          var data = {
             "usertoken": usertoken
          };
          var settings = {
           "async": true,
           "crossDomain": true,
           "url": baseUrl +"/rest/V1/hive/user/set",
           "method": "POST",
           "headers": {
             "content-type": "application/json",
           },
           "processData": false,
           "data": JSON.stringify(data)
          };
          jQuery.ajax(settings).done(function(response){
            console.log('set user token magento',response);
            localStorage.magentoSetToken = true;
          });
        }
    }

    /**
     * Apply Credits function - Magento version
     */
    function applyCredits() {
        var creditapplied = jQuery("#hivecreditamount").val();
        hivePluginCreditApplied = Number(creditapplied.substr(1));
        brand_credits_applied = hivePluginCreditApplied;

        // Disable the button
        jQuery("#hiveApplyCredits").prop('disabled', true);
        jQuery(".hiveError").html(" ");
        jQuery(".hiveSuccess").html("Please wait...");

        var settings = {
           "async": true,
           "crossDomain": true,
           "url": baseUrl + "/rest/V1/carts/mine",
           "method": "POST",
           "headers": {
             "content-type": "application/json",
             "cache-control": "no-cache",
           },
           "processData": false,
           "data": JSON.stringify({})
         }

         // Obtain the cart ID
         jQuery.ajax(settings).done(function (cartId) {
           var discount = hivePluginCreditApplied;
           var data = {
             "discount": discount,
             "usertoken": userToken,
             "cartId": cartId
           };
           var settings = {
             "async": true,
             "crossDomain": true,
             "url": baseUrl + "/rest/V1/hive/discount/apply",
             "method": "POST",
             "headers": {
               "content-type": "application/json",
               "cache-control": "no-cache",
             },
             "processData": false,
             "data": JSON.stringify(data)
           }

           jQuery.ajax(settings).done(function (response) {
             if (response !== "not authorized") {
               jQuery(".hiveError").html(" ");
               jQuery(".hiveSuccess").html("Credits Applied");
               location.reload();
             } else {
               jQuery(".hiveError").html(response);
               jQuery(".hiveSuccess").html("");
             }
           });
         })
         .error(function(jqXHR, textStatus, error) {
           console.log(error);
           console.log(textStatus);
           jQuery(".hiveError").html("There was an error trying to <br />comunicate with the merchant. <br />Try again later.");
           jQuery(".hiveSuccess").html("");
         });
    }

    function isValidString(queryString){
        if(typeof (queryString) !== "undefined" && queryString !== "undefined" && queryString !== null && queryString.length > 0){
            return true;
        }
        return false;
    }

    var cloudPosition = {
      setPosition : function() {
        jQuery(".cloud_image").css({
          'cursor': "pointer",
          'background-size': "contain",
          'background-repeat': "no-repeat",
          'height': "170px",
          'width': "200px"
        });

        jQuery("#hiveText").css({
            'position': "absolute",
            'top': "42%",
            'left': "42%",
            'width': "138px",
            'margin-left': "-40px",
            'margin-top': "-30px",
            'font-size': "70%"
        });

        jQuery("#hiveThinks").css({
          'position': "fixed",
        });

        var positions = getLogoPositions();
        var caseUsed = positions.caseUse;
        var top = Number(jQuery("#hive_display_logo").offset().top - 170);
        var left = Number(jQuery("#hive_display_logo").position().left - 138);


        switch (caseUsed) {
          case 1:
            if (left <= 0) {
              left = 0;
            }
            jQuery("#hiveThinks").css({"transform":'scale(1,1)translateX(13%)translateY(10%)'}); // .style.transform = 'scale(1,1)translateX(75%)translateY(10%)';
            jQuery("#hiveText").css({"transform":'scale(1,1)'}); //.style.transform = 'scale(1,1)';
            jQuery("#hiveText").css({"left":'42%'}); //.style.transform = 'scale(1,1)';

          break;

          case 2:
            left -= 80;
            jQuery("#hiveThinks").css({"transform":'scale(-1,1)translateX(-10%)translateY(10%)'});
            jQuery("#hiveText").css({"transform":'scale(-1,1)', "left": '32%'});

          break;

          case 3:
            left -= 80;
            jQuery("#hiveThinks").css({"transform":'scale(-1,1)translateY(10%)'});
            jQuery("#hiveText").css({"transform":'scale(-1,1)', "left": '32%'});

          break;

          case 4:
            if (left <= 0) {
              left = 0;
            }
            jQuery("#hiveThinks").css({"transform":'scale(1,1)translateX(75%)translateY(10%)'}); // .style.transform = 'scale(1,1)translateX(75%)translateY(10%)';
            jQuery("#hiveText").css({"transform":'scale(1,1)' ,"left":'42%'}); //.style.transform = 'scale(1,1)';

          break;

          case 5:
            left -= 80;
            jQuery("#hiveThinks").css({"transform":'scale(-1,1)translateY(10%)'});
            jQuery("#hiveText").css({"transform":'scale(-1,1)', "left": '32%'});

          break;

          case 6:
            if (left <= 0) {
              left = 0;
            }
            jQuery("#hiveThinks").css({"transform":'scale(1,1)translateX(10%)translateY(10%)'}); // .style.transform = 'scale(1,1)translateX(75%)translateY(10%)';
            jQuery("#hiveText").css({"transform":'scale(1,1)' ,"left":'42%'}); //.style.transform = 'scale(1,1)';

          break;

          case 7:
            left -= 80;
            jQuery("#hiveThinks").css({"transform":'scale(-1,1)translateY(10%)'});
            jQuery("#hiveText").css({"transform":'scale(-1,1)', "left": '32%'});

          break;

          case 8:
            left -= 80;
            jQuery("#hiveThinks").css({"transform":'scale(-1,1)translateY(10%)'});
            jQuery("#hiveText").css({"transform":'scale(-1,1)', "left": '32%'});

          break;

          case 9:
            if (left <= 0) {
              left = 0;
            }
            jQuery("#hiveThinks").css({"transform":'scale(1,1)translateX(10%)translateY(10%)'}); // .style.transform = 'scale(1,1)translateX(75%)translateY(10%)';
            jQuery("#hiveText").css({"transform":'scale(1,1)' ,"left":'42%'}); //.style.transform = 'scale(1,1)';

          break;

          default:
            if (left <= 0) {
              left = 0;
            }
            jQuery("#hiveThinks").css({"transform":'scale(1,1)translateX(13%)translateY(10%)'}); // .style.transform = 'scale(1,1)translateX(75%)translateY(10%)';
            jQuery("#hiveText").css({"transform":'scale(1,1)'}); //.style.transform = 'scale(1,1)';
            jQuery("#hiveText").css({"left":'42%'}); //.style.transform = 'scale(1,1)';

          break;
        }

        jQuery("#hiveThinks").css({
            'top': top + "px",
            'left': left + "px"
        });

      }
    }

    var popup = {
      open : function() {
        // Create the cloud with its contets
        var cloudThinks = "";
        if (!isValidString(userToken)) {
            cloudThinks = 'Earn <span class="earningstyle">'+earning_percent_initial+"%</span> in store credit on this purchase by signing up for Hive";
        } else {
            cloudThinks = 'You have <span class="earningstyle"> $ '+Number(credits_applied).toFixed(2)+"</span> of credit at this store";
        }

        // Add the attachment for the event
        jQuery("body").on("click",".cloud_image", function() {
            getUserAppliedCredits();
            jQuery("#hiveThinks").fadeToggle();
            jQuery(".hivedialog").fadeToggle();
        });

        // watch the resize to recalibrate the position
        jQuery(window).resize(function() { cloudPosition.setPosition(); });

        jQuery("body").append(getTemplate(templates.cloudHive).render({HiveThinks: cloudThinks}));

        if (!isValidString(userToken)) {
          // Set the timeout
          setTimeout(function() {
            cloudPosition.setPosition();
            jQuery("#hiveThinks").show();
            return setTimeout(function() { popup.close(); }, 10000);
          }, 5000);
        }
      },
      close : function() {
        setTimeout(function(){
          jQuery("#hiveThinks").hide();
        }, 3000);
      }
    }

    function getUserAppliedCredits() {
      jQuery.get(serverapi+'/shopifyapi/getUserAppliedCredits?token='+userToken+"&brandurl="+urlHost,function(appliedCreditResponse) {
        if (appliedCreditResponse.error) {
          // Remove the plugin authentication
          localStorage.removeItem("hivePlugin");
          earning_percent_initial = appliedCreditResponse.percent;
          popup.open();
          console.log("Display plugin pop");
          return loadLoginDisplay(earning_percent_initial,bodycolor,textcolor,earningcolor,headerbuttoncolor,submitbuttoncolor);
        }
        credits_applied = appliedCreditResponse.credits_applied;
        return loadPluginDisplay(userToken,bodycolor,textcolor,earningcolor,headerbuttoncolor,submitbuttoncolor, pluginposition, appliedCreditResponse);
      });
    } // End get User Applied Credits function

    function displayLogo(logoUrl, pluginposition) {
        jQuery("body").append(getTemplate(templates.hiveLogo).render({ logoHiveUrl: logoUrl}));

        // Add the event listener
        jQuery("#hive_display_logo").click(function(e){
            var dropdowncontainer = jQuery(".hive-plugin-data");
            var logoContainer = jQuery("#hive_display_logo");
            if (!dropdowncontainer.is(e.target) // if the target of the click isn't the container...
             && dropdowncontainer.has(e.target).length === 0
             && !logoContainer.is(e.target)) // ... nor a descendant of the container
            {
                dropdowncontainer.fadeToggle();
            }
        });

        checkIfLogoIsLoaded();

        function checkIfLogoIsLoaded() {
            if (jQuery("#hive_display_logo").width()===60 || jQuery( "#hive_display_logo" ).width()===0) {
                setTimeout(checkIfLogoIsLoaded, 500);
            } else {
                setLogoPosition(pluginposition);
                setTimeout(function() {jQuery("#hive_display_logo").show();}, 500);
            }
        }
    }

    // Returns the infromation from the server for the merchant configuration
    function getPluginDetail() {
       jQuery.get(serverapi+'/shopifyapi/getPluginDetail?brandurl='+urlHost, function(layout) {

          if(layout.success) {
              // Load the styles configuration
              logoUrl = contenturl+"hivelogo/"+(layout.brandlogo || 'hivelogo.png');
              bodycolor = layout.bodycolor || '#56dbff';
              textcolor = layout.textcolor || '#000000';
              earningcolor = layout.earningcolor || '#ffffff';
              headerbuttoncolor = layout.headerbutton || "#0071bc";
              submitbuttoncolor = layout.submitbutton || '#ffdf7f';
              pluginposition = layout.pluginposition || 0;

              getUserAppliedCredits();

              displayLogo(logoUrl, pluginposition);
              jQuery("body").append(getTemplate(templates.hPRegister).render({bodyColor: bodycolor, textColor: textcolor, displaybody: bodyDisplay}));
              jQuery(".hivedialog").fadeToggle();

              // Validates if the user is logged in
              if(isValidString(userToken)) {
                  if (typeof(saveUserTokenToMagento) === 'function') {
                      saveUserTokenToMagento(userToken);
                  }
              }

              jQuery("body").on("click", "#hive_display_logo", function() {
                  setHiveDialogPosition(pluginposition);
                  jQuery(".hivedialog").fadeToggle();
                  jQuery("#hiveThinks").hide();
              });
          } else {
              console.log("Could not load the plugin as this store is not registered as a Merchant");
          }
      });
    } // End get plugin details


     //Method called if user is not logged in to Hive or has not registered.
    function loadLoginDisplay(initial_earning_percent, bodycolor, textcolor, earningcolor, headerbuttoncolor, submitbuttoncolor, pluginposition, bodyDisplay) {
         var content = getTemplate(templates.login).render({
              textColor: textcolor || "#ffffff",
              bodyColor: bodycolor,
              headerButtonColor: headerbuttoncolor,
              earningColor: earningcolor,
              initialEarningPercent: initial_earning_percent,
              submitButtonColor: submitbuttoncolor
          });

          displayPluginData(content,pluginposition);

          jQuery(".accepttnc").click(function(){
             window.open("https://www.hivetogether.com/userterms");
          });

          /**
           * Call facebook login
           */
          jQuery(".fb-img-container").click(function(){

              //var redirectUri="https://www.hivetogether.com/fbauth";
              var redirectUri = 'https://5e556f6d.ngrok.io/fbauth';
              var fbUrl = "https://www.facebook.com/dialog/oauth?client_id="+facebookclient+"&redirect_uri="+redirectUri+"&scope=email";
              var newWin = window.open(fbUrl);


              function receiveMsg(e){
                  var key = e.message ? "message" : "data";

                  if (e[key].success) {

                      userToken = e[key].usertoken;
                      localStorage.hivePlugin=userToken;
                      loadPluginDisplay(userToken);
                      //console.log("Token: " + userToken); //Uncomment if you want to see token
                      newWin.close();
                  }
              };

              if(window.addEventListener){
                  addEventListener("message", receiveMsg, false);
              }else{
                  attachEvent("onmessage", receiveMsg);
              };

          });

          jQuery(".loginButton").click(function(){

              jQuery(".loginButton").css({"background":bodycolor});
              jQuery(".registerButton").css({"background":headerbuttoncolor});
              // jQuery(".plugin-login-text-body").show();
              // jQuery(".plugin-register-text-body").hide();
              jQuery("#plugin-login").show();
              jQuery("#plugin-register").hide();
          });
          jQuery(".registerButton").click(function(){

              jQuery(".registerButton").css({"background":bodycolor});
              jQuery(".loginButton").css({"background":headerbuttoncolor});
              // jQuery(".plugin-login-text-body").hide();
              // jQuery(".plugin-register-text-body").show();
              jQuery("#plugin-login").hide();
              jQuery("#plugin-register").show();


          });



          //On form submit

          var newWin;

          jQuery(".reg-submit-btn").click(function(){

              var tncChecked=jQuery(".plugintnc").prop('checked');
              var username = jQuery(".hive-reg-username").val();
              var password = jQuery(".hive-reg-userpassword").val();
              var email = jQuery(".hive-reg-useremail").val();

              if(username.length === 0 || username === null ){
                  jQuery(".plugin-error-message").html("Username cannot be empty");
              } else if(password.length===0 || password === null){
                  jQuery(".plugin-error-message").html("Password cannot be empty");
              } else if(email.length===0 || username===null){
                  jQuery(".plugin-error-message").html("Email cannot be empty");
              } else if(tncChecked===false){
                  jQuery(".plugin-error-message").html("Please accept TnC");
              } else {
                  jQuery(".plugin-error-message").html(" ");
                  var serverurl = serverapi + "/shopifyapi/registerViaPlugin";
                  var data = {"username": username, "password": password, "email": email};
                  jQuery.post(serverurl, data, function (response) {
                      if (response.success) {
                          userToken = response.token;

                          localStorage.hivePlugin = userToken;
                          // Save this on local storage
                          scriptUrl = serverapi;
                          if (!hasRequireJs) {
                            scriptUrl += '/client.min.js';
                          } else {
                            scriptUrl += '/scripts/client.no-require.js';
                          }
                          loadScript(scriptUrl, function() {
                            try {
                              var storage = new CrossStorageClient(serverapi+'/hub');
                              storage.onConnect().then(function() {
                                // Try to see if there is a session information stored
                                return storage.set('userToken', userToken);
                              }).then(function() {
                                getUserAppliedCredits();
                              })['catch'](function(err) {
                                console.log("Error on cross-storage query: " + err);
                                getUserAppliedCredits();
                              });
                            } catch (error) {
                                console.log("Could not load the CrossStorageClient: " + error);
                                getUserAppliedCredits();
                            }
                          });
                      } else {
                          jQuery(".hive-plugin-error").html("Unable to register User");
                      }
                });
              }
          });

          //On login submit
          jQuery(".login-submit-btn").click(function(){

              var email=jQuery(".hive-login-useremail").val();
              var password=jQuery(".hive-login-userpassword").val();

              var serverurl=serverapi+"/shopifyapi/loginViaPlugin";
              var data={"email":email,"password":password};

              jQuery.post(serverurl,data,function(loginResponse){
                  if(loginResponse.success){
                      userToken = loginResponse.token;
                      localStorage.hivePlugin = userToken;
                      // Save this on local storage
                      scriptUrl = serverapi;
                      if (!hasRequireJs) {
                        scriptUrl += '/client.min.js';
                      } else {
                        scriptUrl += '/scripts/client.no-require.js';
                      }
                      loadScript(scriptUrl, function() {
                        try {
                          var storage = new CrossStorageClient(serverapi+'/hub');
                          storage.onConnect().then(function() {
                            // Try to see if there is a session information stored
                            return storage.set('userToken', userToken);
                          }).then(function() {
                            getUserAppliedCredits();
                          })['catch'](function(err) {
                            console.log("Error on cross-storage query: " + err);
                            getUserAppliedCredits();
                          });
                        } catch (error) {
                          console.log("Could not load the CrossStorageClient: " + error);
                          getUserAppliedCredits();
                        }
                      });
                  } else {
                      jQuery(".hive-plugin-error").html("Unable to login.");
                  }
              });
          });

          jQuery(".fb-login").click(function(){
              var fbUrl="https://www.facebook.com/dialog/oauth?client_id="+facebookclient+"&redirect_uri=http://localhost:4200/pluginfblogin";
              window.open(fbUrl);
          });

      }

    //Method called if user is logged in and plugin is displayed
    function loadPluginDisplay(usertoken,bodycolor,textcolor,earningcolor,headerbuttoncolor,submitbuttoncolor,pluginposition,appliedCreditResponse){
        jQuery(".hive-plugin-data").css({"height":"auto","border-radius":"35px"});

        var content="";

        var pluginDataUrl = serverapi+'/magento/credits/applied?token='+usertoken+"&brandurl="+host;
        jQuery.get(pluginDataUrl,function(appliedCreditResponse) {

            if(appliedCreditResponse.isFavorite) {
                isBrandFavorite = true;
            }else{
                isBrandFavorite = false;
            }
            brand_credits_applied = Number(appliedCreditResponse.credits_applied).toFixed(2);

            jQuery.get(serverapi + '/shopifyapi/getUserDetail?token=' + usertoken + "&brandurl=" + host, function (data) {
                var brandImgLocation = data.brandlogo;
                if(typeof brandImgLocation === "undefined"
                || brandImgLocation === null
                || brandImgLocation.length === 0) {
                    brandImgLocation = "images/brand_imgs/bumblebee.png";
                }
                if (!isValidString(data.username)) {
                    data.username = 'you...';
                }

                var content = getTemplate(templates.hPDisplay_header).render({ userName: data.username});

                ///*************************LOAD TEMPLATE ACCORD USER DETAIL*******************************///
                if (data.activecredits) {
                    var earn="";
                    var favorite="res/notfavorite.png";
                    if(!appliedCreditResponse.isFavorite && appliedCreditResponse.favorite_earning>appliedCreditResponse.initial_earning) {
                        earn="to earn XX%";
                    }else if(!appliedCreditResponse.isFavorite){
                        favorite="res/notfavorite.png";
                        earn="";
                    }else{
                        favorite="res/favorite.png";
                        earn="";
                    }


                    var creditsActive=getTemplate(templates.hPDisplay_cActive).render({
                        brandName: data.brandname,
                        serverApi: serverapi,
                        brandImageLocation: brandImgLocation,
                        activeCredits: data.activecredits,
                        earningPercent: data.earningpercent,
                        submitButtonColor: submitbuttoncolor,
                        contentUrl: contenturl,
                        fav: favorite,
                        toEarn: earn
                    });

                    content += creditsActive;
                } else if (data.hivecredits && typeof(data.hivecredits) !== "undefined" && data.hivecredits.length > 0)  {
                    var earn="";
                    var favorite="";
                    if(!appliedCreditResponse.isFavorite && appliedCreditResponse.favorite_earning>appliedCreditResponse.initial_earning) {
                        favorite="res/notfavorite.png";
                        earn="to earn XX%";
                    }else if(!appliedCreditResponse.isFavorite){
                        favorite="res/notfavorite.png";
                        earn="";
                    }else{
                        favorite="res/favorite.png";
                        earn="";
                    }

                    var span="";
                    data.hivecredits.forEach(function (credit) {
                        span+="<span class='plugin-hivemember' style='color: "+earningcolor+"'>"+credit.membername+"</span>";
                     });


                    var creditsHive = getTemplate(templates.hPDisplay_cHive).render({
                        brandName: data.brandname,
                        serverApi: serverapi,
                        brandImageLocation: brandImgLocation,
                        spanCredits: span,
                        earningColor: data.earningcolor,
                        earningPercent: data.earningpercent,
                        contentUrl: contenturl,
                        fav: favorite,
                        toEarn: earn
                    });

                    content  += creditsHive;

                } else {
                    var earn="";
                    var favorite="";
                    if( !appliedCreditResponse.isFavorite
                      && appliedCreditResponse.favorite_earning > appliedCreditResponse.initial_earning) {
                        favorite="res/notfavorite.png";
                        earn="to earn XX%";
                    } else if(!appliedCreditResponse.isFavorite) {
                        favorite="res/notfavorite.png";
                        earn="";
                    } else {
                        favorite="res/favorite.png";
                        earn="";
                    }


                    var creditsZero = getTemplate(templates.hPDisplay_cZero).render({
                        brandName: data.brandname,
                        serverApi: serverapi,
                        brandImageLocation: brandImgLocation,
                        earningColor: data.earningcolor,
                        earningPercent: data.earningpercent,
                        contentUrl: contenturl,
                        fav: favorite,
                        toEarn: earn
                    });

                    content  += creditsZero;

                }
                ///*************************END LOAD TEMPLATE*******************************///

                displayPluginData(content,pluginposition);

                //Plugin Actions


                // Display the text field

                jQuery("#textFieldCredit").show();
                jQuery("#hiveCreditsInput").val(setCurrencyFormat(String(brand_credits_applied)));
                jQuery("#hivecreditamount").val("$"+setCurrencyFormat(String(brand_credits_applied)));

                jQuery("#hiveCreditsInput").on('input', function() {

                    jQuery(".hiveError").html("");
                    jQuery(".hiveSuccess").html("");

                    var value = this.value;

                    // store current cursor positions
                    var start = this.selectionStart;
                    var end = this.selectionEnd;

                    value = setCurrencyFormat(value);

                    this.value = value;
                    jQuery("#hivecreditamount").val("$ " + value);

                    //restore cursor positions
                    this.setSelectionRange(start, end);

                });




                ///*********************Old implementation (with slider or text field)**************************///
                /*


                checkIfSliderIsLoaded();

                function checkIfSliderIsLoaded() {
                    if (jQuery("#slider").width() === 100 || jQuery( "#slider" ).width()===0) {
                        setTimeout(checkIfSliderIsLoaded, 500);
                    } else {
                        //Apply credits (slider or texfield)
                        var step=0.05;
                        var maxPxBetweenSteps=2;

                        var sliderWidth=jQuery("#slider").width();
                        //Total width[px]/Total steps  (px between steps):
                        var p=sliderWidth*step/data.activecredits;
                        if( p >maxPxBetweenSteps) {
                            // Display the slider

                            var creditApplySlider = jQuery("#slider").slider({
                                step: step,
                                min: 0,
                                max: data.activecredits
                            });

                            if(typeof brand_credits_applied!="undefined"
                            && typeof creditApplySlider!="undefined") {

                                jQuery("#hivecreditamount").val("$"+brand_credits_applied);

                                creditApplySlider.slider('setValue', Number(brand_credits_applied));

                                creditApplySlider.change(function () {

                                    var value = creditApplySlider.slider('getValue');

                                    jQuery("#hivecreditamount").val("$ " + value);

                                });
                            }
                        } else {
                            // Display the text field

                            jQuery("#textFieldCredit").show();
                            jQuery("#hiveCreditsInput").val(setCurrencyFormat(String(brand_credits_applied)));
                            jQuery("#hivecreditamount").val("$"+setCurrencyFormat(String(brand_credits_applied)));

                            jQuery("#hiveCreditsInput").on('input', function() {

                                jQuery(".hiveError").html("");
                                jQuery(".hiveSuccess").html("");

                                var value=this.value;

                                // store current cursor positions
                                var start = this.selectionStart;
                                var end = this.selectionEnd;

                                value=setCurrencyFormat(value);

                                this.value=value;
                                jQuery("#hivecreditamount").val("$ " + value);

                                //restore cursor positions
                                this.setSelectionRange(start, end);

                            });
                        }
                    }
                }

                */
                ///*******************************************************************************************///
                jQuery(".pluginfavoriteimg").hover(


                    function () {

                        if(!isBrandFavorite) {
                            jQuery(this).attr("src", contenturl + "res/add_favorite.png");
                        }else{
                            jQuery(this).attr("src", contenturl + "res/remove_favorite.png");
                        }
                    },

                    function () {
                        if(!isBrandFavorite) {
                            jQuery(this).attr("src", contenturl + "res/notfavorite.png");
                        }else{
                            jQuery(this).attr("src",contenturl+"res/favorite.png");
                        }
                    });

                jQuery(".pluginfavoriteimg").click(function(){
                    if(!isBrandFavorite) {
                        jQuery(this).attr("src", contenturl + "res/favorite.png");
                        isBrandFavorite=true;
                        var add_data={'brand':urlHost,'user':usertoken};
                        jQuery.post(serverapi+"/shopifyapi/addFavorite",add_data,function(response){

                        });

                    }else{
                        jQuery(this).attr("src", contenturl + "res/notfavorite.png");
                        isBrandFavorite=false;
                        var remove_data={'brand':urlHost,'user':usertoken};
                        jQuery.post(serverapi+"/shopifyapi/removeFavorite",remove_data,function(response){

                        });

                    }
                });

                var hivePluginCreditApplied = Number(brand_credits_applied);

                var clear_data = {
                    'usertoken': usertoken,
                    'brand': host,
                    'creditsapplied': hivePluginCreditApplied
                };

                jQuery("#hiveApplyCredits").click(function() {
                    jQuery(".hiveError").html("");
                    jQuery(".hiveSuccess").html("");
                    var creditapplied = jQuery("#hivecreditamount").val();
                    creditapplied = creditapplied.replace(/,/g,"");
                    creditapplied = Number(creditapplied.substr(1));

                    if(creditapplied > data.activecredits) {
                        jQuery(".hiveError").html("Credit amount applied exceeds credit available.");
                    } else {
                        applyCredits();
                    }
                });
                jQuery("#hiveClearCredits").click(function() { clearCredits(clear_data) });
            });
        });
    }

    function setCurrencyFormat(value) {

        var cents="", parts, output=[], formatted; //auxiliary variables

        if(value.indexOf(".") > 0) {
            parts = value.split(".");
            value = parts[0];
            cents = parts[1];
        }

        //Delete non numeric characters
        value = value.replace(/\D/g,'');
        cents = cents.replace(/\D/g,'');

        if(value === ""){
            value = "0";
        }

        while(value.length>1 && value.charAt(0)==="0"){
            value=value.substr(1);
        }

        cents=cents.substr(0, 2);

        //Add commas
        value = value.split("").reverse();
            for(var j = 0, i=1, len = value.length; j < len; j++) {
                if(value[j] !== ",") {
                    output.push(value[j]);
                    if(i%3 === 0 && j < (len - 1)) {
                        output.push(",");
                    }
                i++;
            }
        }

        formatted = output.reverse().join("");

        return (formatted + ((parts) ? "." + cents : ""));
    }

    function setLogoPosition(pluginposition) {

        var hiveLogo = jQuery("#hive_display_logo");
        var positions = getLogoPositions();
        hiveLogo.css("left", positions.position.left+"%");
        hiveLogo.css("bottom", positions.position.bottom+"%");
        hiveLogo.css("margin-left", positions.margin.left+"px");
        hiveLogo.css("margin-bottom", positions.margin.bottom+"px");
    }

    function getLogoPositions() {

        var hiveLogo = jQuery( "#hive_display_logo" );
        var logoWidth = hiveLogo.width();
        var logoHeight = hiveLogo.height();
        //Logo properties:
        var logoPositionLeft = '0';
        var logoPositionBottom = '0';
        var logoMarginLeft = '0';
        var logoMarginBottom = '0';
        var caseUse = '0';

        switch(pluginposition) {
          case 1:
              logoPositionLeft=0;
              logoPositionBottom=0;
              logoMarginLeft=0;
              logoMarginBottom=0;
              caseUse = 1;
              break;
          case 2:
              logoPositionLeft=50;
              logoPositionBottom=0;
              logoMarginLeft=String(-logoWidth/2.0);
              logoMarginBottom=0;
              caseUse = 2;
              break;
          case 3:
              logoPositionLeft=100;
              logoPositionBottom=0;
              logoMarginLeft=String(-logoWidth);
              logoMarginBottom=0;
              caseUse = 3;
              break;
          case 4:
              logoPositionLeft=15;
              logoPositionBottom=0;
              logoMarginLeft=0;
              logoMarginBottom=0;
              caseUse = 4;
              break;
          case 5:
              logoPositionLeft=85;
              logoPositionBottom=0;
              logoMarginLeft=String(-logoWidth);
              logoMarginBottom=0;
              caseUse = 5;
              break;
          case 6:
              logoPositionLeft=0;
              logoPositionBottom=15;
              logoMarginLeft=0;
              logoMarginBottom=0;
              caseUse = 6;
              break;
          case 7:
              logoPositionLeft=100;
              logoPositionBottom=15;
              logoMarginLeft=String(-logoWidth);
              logoMarginBottom=0;
              caseUse = 7;
              break;
          case 8:
              logoPositionLeft=100;
              logoPositionBottom=50;
              logoMarginLeft=String(-logoWidth);
              logoMarginBottom=String(-logoHeight/2.0);
              caseUse = 8;
              break;
          case 9:
              logoPositionLeft=0;
              logoPositionBottom=50;
              logoMarginLeft=0;
              logoMarginBottom=String(-logoHeight/2.0);
              caseUse = 9;
              break;
          default:
              logoPositionLeft=0;
              logoPositionBottom=0;
              logoMarginLeft=0;
              logoMarginBottom=0;
              caseUse = 99;
        }

        return {
            position: {
              left: logoPositionLeft,
              bottom: logoPositionBottom
            },
            margin: {
              left: logoMarginLeft,
              bottom: logoMarginBottom
            },
            caseUse: caseUse
        }
    }

    function displayPluginData(content, pluginposition) {
        jQuery(".hive-plugin-data").html(content);
        setHiveDialogPosition(pluginposition);
        // Handle resize of the window to reposition logo
        jQuery(window).resize(function() { setHiveDialogPosition(pluginposition); });
        // Show the contents of the elemnt to have them ready
        jQuery(".hive-plugin-data").show();
    }

    function setHiveDialogPosition(pluginposition) {
        //Hive logo properties:
        var hiveLogo = jQuery("#hive_display_logo");
        var logoPositionLeft = hiveLogo.css("left");
        var logoPositionBottom = hiveLogo.css("bottom");
        var logoWidth = hiveLogo.width();
        var logoHeight = hiveLogo.height();

        //Body properties:
        var body = jQuery( "body" );
        var bodyWidth = body.width();
        var bodyHeight = body.height() - body.offset().top;

        //Hive Dialog properties:
        var hiveDialog=jQuery("#register");
        var hiveDialogWidth = hiveDialog.width();
        var hiveDialogHeight = hiveDialog.height();
        var hiveDialogMarginLeft = '0px';
        var hiveDialogMarginBottom = '0px';
        var hiveDialogPositionLeft = '0px';
        var hiveDialogPositionBottom = '0px';

        if (window.matchMedia('(max-width: 480px)').matches) {

            hiveDialogPositionLeft='0%';
            hiveDialogPositionBottom='0%';
            hiveDialogMarginLeft='2%';

            switch(pluginposition) {
            case 1:
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 2:
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 3:
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 4:
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 5:
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 6:
                hiveDialogPositionBottom=logoPositionBottom;
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 7:
                hiveDialogPositionBottom=logoPositionBottom;
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 8:
                hiveDialogMarginBottom='20%';
                break;
            case 9:
                hiveDialogMarginBottom='20%';
                break;
            default:
                hiveDialogMarginBottom= String(logoHeight/2)+'px';
                break;
          }
        } else {

          hiveDialogPositionLeft=logoPositionLeft;
          hiveDialogPositionBottom=logoPositionBottom;

          switch(pluginposition) {
            case 1:
                hiveDialogMarginLeft= String(logoWidth/2)+'px';
                hiveDialogMarginBottom= String(logoHeight/3)+'px';
                break;
            case 2:
                hiveDialogMarginLeft=String(-hiveDialogWidth/2.0)+'px';
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 3:
                hiveDialogMarginLeft=String(-hiveDialogWidth-logoWidth/2)+'px';
                hiveDialogMarginBottom=String(logoHeight/3)+'px';
                break;
            case 4:
                hiveDialogMarginLeft=String(-hiveDialogWidth/5)+'px';;
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 5:
                hiveDialogMarginLeft=String(-hiveDialogWidth+hiveDialogWidth/5)+'px';
                hiveDialogMarginBottom=String(logoHeight/2)+'px';
                break;
            case 6:
                hiveDialogMarginLeft=String(logoWidth*3/5)+'px';
                hiveDialogMarginBottom=String(-hiveDialogHeight/6)+'px';
                break;
            case 7:
                hiveDialogMarginLeft=String(-hiveDialogWidth-logoWidth*4/5)+'px';
                hiveDialogMarginBottom=String(-hiveDialogHeight/6)+'px';
                break;
            case 8:
                hiveDialogMarginLeft=String(-hiveDialogWidth-logoWidth*4/5)+'px';
                hiveDialogMarginBottom=String(-hiveDialogHeight/2-logoHeight/5)+'px';
                break;
            case 9:
                hiveDialogMarginLeft=String(logoWidth*3/5)+'px';
                hiveDialogMarginBottom=String(-hiveDialogHeight/2-logoHeight/5)+'px';
                break;
            default:
                hiveDialogMarginLeft= String(logoWidth/2)+'px';
                hiveDialogMarginBottom= String(logoHeight/5)+'px';
                break;
          }
        }

        hiveDialog.css("left",hiveDialogPositionLeft);
        hiveDialog.css("bottom",hiveDialogPositionBottom);
        hiveDialog.css("margin-left",hiveDialogMarginLeft);
        hiveDialog.css("margin-bottom",hiveDialogMarginBottom);
    }

    //Function defintions got plugin cart interactions
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

  // Init the process of the application
      getPluginDetail();
    }; // End application
  /* If jQuery UI has not yet been loaded or if it has but it's too old for our needs,
   we will load jQuery from the Google CDN, and when it's fully loaded, we will run
   our app's JavaScript. Set your own limits here, the sample's code below uses 1.7
   as the minimum version we are ready to use, and if the jQuery is older, we load 1.9. */
   function loadApp() {
     loadScript('https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.81/jsrender.js', function() {
         if (!hasBootstrapSlider) {
            loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/6.0.16/bootstrap-slider.min.js', function(){
                app = myAppJavaScript();
            });
         } else {
           app = myAppJavaScript();
         }
     });
  }
}());
