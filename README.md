# Hive Magento 2.x Module Installation Instructions

## Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Troubleshooting](#troubleshooting)

## Prerequisites

Before installing the Hive module you should ensure that your
Magento2 store is running without any problems in your environment.

Hive Module supports Magento2 Community and Enterprise Edition from
version 2.0 onwards.

You need an account with [Hive Merchants](https://merchants.hivetogether.com/)
which allows to include the plugin into your store.
If you need professional services for assistance with setting up your
environment please contact selena@hivetogether.com.

## Installation

The installation of the Magento module is pretty easy and can be performed in
a few ways depending on your Magento version.

- [Install from Magento Marketplace (only Magento 2.0.x versions)](#installing-from-the-magento-marketplace-using-web-setup-wizard)
- [Install using Composer](#installing-using-composer)
- [Install from Zip file](#installing-from-zip-file)

#### Installing from the Magento Marketplace using Web Setup Wizard (only Magento 2.0.x version)

##### Note:

_This installation method works only on **2.0.x** versions of Magento. If you're using a newer,
**2.1.x** version of Magento, please install the extension [using Composer](#installing-using-composer) or
[install from Zip file.](#installing-from-zip-file)_

This will require an account with Magento Commerce and the associated
[API keys](http://devdocs.magento.com/guides/v2.0/install-gde/prereq/connect-auth.html)
will be used to sync with the marketplace.

1. Open a browser to the [Magento Marketplace](https://marketplace.magento.com/)
   and add the module to the cart. Check out and ensure that this is added to
   your account.
2. Log into the admin section of the Magento system in which to install the
   module as an administrator.
3. Start the Web Setup Wizard by navigating to 'System > Web Setup Wizard'.
4. Click 'Component Manager' to synchronise with Magento Marketplace.
5. Click to 'Enable' HiveTogether.
6. Enter to your frontend stores and you should see the plugin been load automatically.

#### Installing using Composer

1. Login (or switch user) as the Magento filesystem owner.
1. Ensure that the files in 'app/etc' under the Magento root are write enabled
    by the Magento Filesystem owner.
1. Ensure that Git and Composer are installed.
1. Next fetch the module with:

    ```
    composer require hive-together/hive-merchant-magento
    ```

1. Once the module fetch has completed enable it by:

    ```
    bin/magento module:enable Hive_HiveMerchant
    ```

1. Then finally the clean up tasks:

    ```
    bin/magento setup:upgrade
    ```

    followed by:

    ```
    bin/magento cache:clean
    ```

#### Installing from zip file
 Needs modification
1. Open a browser to [Bitbucket](https://bitbucket.org/ashelm/hive-magento-plugin/downloads?tab=tags)
    note/copy the URL of the version to install (highest version available).

2. Log in to the Magento server as the Magento filesystem owner and navigate to
    the Magento Home directory.

3. Create a directory `<magento home>/app/code/HiveTogether/HiveMerchant/` and change directory
    to the new directory.

4. Download the zip/tarball and decompress it.

5. At this point, it is possible to install with either the:

   **Web Setup Wizard's Component Manager (only Magento 2.0.x versions, for 2.1.x use command line)**
   1. To install in the Web Setup Wizard. Open a browser and log in to the Magento
       admin section with administrative privileges.
   2. Navigate to 'System > Web Setup Wizard'.
   3. Click 'Component Manager' scroll down and locate 'HiveMerchant'. Click enable
       on the actions.
   4. Follow the on screen instructions ensuring to create backups.

   **Command line**
      1. To enable the module on the command line change directory to the Magento
       Home directory. Ensure you are logged in as the Magento filesystem owner.
      2. Verify that 'HiveMerchant' is listed and shows as disabled: `bin/magento
       module:status`.
      3. Enable the module with: `bin/magento module:enable Hive_HiveMerchant`.
      4. Then we need to ensure the configuration tasks are run: `bin/magento
       setup:upgrade`.
      5. Finally on the command line to clear Magento's cache run: `bin/magento
       cache:clean`.

### Troubleshooting

- Ensure that your Magento version is tested and supported with the version of
  HiveTogether that you are using.

- Ensure that all files and folders have the correct permissions for the
  Magento Filesystem Owner and the Web Server user.

- Ensure that the cache's are cleaned and disable / re-enable the module.

- Ensure that text content is not being compressed.

If any critical issue occurs you can't easily solve, execute
`bin/magento module:disable Hive_HiveMerchant` as the Magento Filesystem Owner to
disable the HiveMerchant module. If necessary clear Magento's cache again.

If possible, gather as much data and open an issue on (bitbucket) to
help improve the module.