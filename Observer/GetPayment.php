<?php

namespace Hive\HiveMerchant\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class GetPayment implements ObserverInterface
{
  protected $catalogSession;
  protected $storeManager;

  protected $hiveAPI;

  public function __construct(\Magento\Catalog\Model\Session $catalogSession,
    \Magento\Store\Model\StoreManagerInterface $storeManager)
  {
    $this->catalogSession = $catalogSession;
    $this->storeManager = $storeManager;
    $this->hiveAPI = 'https://api.hivetogether.com';
  }

  public function execute(Observer $observer)
  {
    $usertoken =  $this->catalogSession->getData('usertoken');
    $order = $observer->getData('payment')->getOrder();
    $customerId = $order->getCustomerId();

    $brandUrl = $this->storeManager->getStore()->getBaseUrl();
    $brandUrl = $this->getBrandUrl($brandUrl);

    // get order
    $orderId = $order->getRealOrderId();
    $data = array(
        'usertoken' => $usertoken,
        'brand' => $brandUrl,
        'order' => $orderId,
        'customer' => $customerId
    );

    // send Hive api request to place order

    $url = $this->hiveAPI .'/magento/purchase/confirm';

    $client = new \Zend_Http_Client($url);
    $client->setHeaders('Content-type','application/json');
    $client->setParameterPost($data);
    $json = $client->request(\Zend_Http_Client::POST);
    $json = $json->getRawBody();
    $json = json_decode($json, true);

    // Remove Cart Rule
    $couponCode = $order->getCouponCode();

    if ($couponCode && strpos($couponCode,'Hive') == 0) {
      $this->deleteHiveCoupon($couponCode);
    }
  }

  /**
   * get the current store brand url
   * @return [type] [description]
   */
  private function getBrandUrl() {
    $url = $this->storeManager->getStore()->getBaseUrl();
    $brandUrl = str_replace('https://', '', $url);
    $brandUrl = str_replace('http://', '', $url);
    // remove the end /
    if (substr($brandUrl, -1) == '/') {
      $brandUrl = substr($brandUrl, 0, (strlen($brandUrl)-1));
    }
    return $brandUrl;
  }

   /**
   * Find a Cart Rule and delete it
   * @param  [type] $couponCode [description]
   * @return [type]             [description]
   */
  private function deleteHiveCoupon($couponCode) {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $salesRule = $objectManager->get('\Magento\SalesRule\Model\Rule');
    $model = $salesRule
    ->getCollection()
    ->addFieldToFilter('name',array('eq'=> sprintf('Hive Discount - %s', $couponCode)))
    ->getFirstItem();

    $model->delete();
  }
}
