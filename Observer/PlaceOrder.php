<?php

namespace Hive\HiveMerchant\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class PlaceOrder implements ObserverInterface
{
  protected $catalogSession;
  protected $storeManager;
  protected $salesRule;
  protected $quoteManagement;

  protected $hiveAPI;

  public function __construct(\Magento\Catalog\Model\Session $catalogSession,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Quote\Model\QuoteManagement $quoteManagement,
      \Magento\SalesRule\Model\Rule $salesRule
    )
  {
    $this->catalogSession = $catalogSession;
    $this->storeManager = $storeManager;
    $this->salesRule = $salesRule;
    $this->quoteManagement = $quoteManagement;
    $this->hiveAPI = 'https://api.hivetogether.com';
  }

  public function execute(Observer $observer)
  {

    try {
      // Get usertoken
      $usertoken =  $this->catalogSession->getData('usertoken');

      $brandUrl = $this->getBrandUrl();

      if (!$usertoken){
        // No hive data
        $usertoken = "No session data. Is not a hive user";

      }
      // get order
      $order = $observer->getData('order');
      // customer id, this will be use to get the proper order when payment is completed
      $customerId = $order->getCustomerId();
      // order id
      $orderId = $order->getRealOrderId();

      // order prices
      $totalPrice = $order->getGrandTotal();
      $purchasePrice = $order->getSubtotal() + $order->getDiscountAmount();

      // generate data to object to send
      $data = array(
          'usertoken' => $usertoken,
          'brand' => $brandUrl,
          'couponcode' => 0,
          'credits' => 0,
          'order' => $orderId,
          'total' => $totalPrice,
          'purchaseprice' => $purchasePrice,
          'customer' => $customerId
      );

      // get coupon code data
      $couponCode = $order->getCouponCode();
      // Check if it is a hive coupon. Hive coupons codes starts with Hive
      if ($couponCode && strpos($couponCode,'Hive') == 0) {
        $rulesIds = $order->getAppliedRuleIds();
        // add coupon data to request
        $data['couponcode'] = $couponCode;
        $data['credits'] = $this->getHiveDiscount($rulesIds, $couponCode);

      }
      // place order in Hive
      $url = $this->hiveAPI . '/magento/order/place';
      $client = new \Zend_Http_Client($url);
      $client->setHeaders('Content-type','application/json');
      $client->setParameterPost($data);
      $json = $client->request(\Zend_Http_Client::POST);
      $json = $json->getRawBody();
      $json = json_decode($json, true);
    } catch(\Exception $e) {
       return 'error';
    }

  }

  /**
   * Find a Cart Rule and delete it
   * @param  [type] $couponCode [description]
   * @return [type]             [description]
   */
  private function deleteHiveCoupon($couponCode) {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $salesRule = $objectManager->get('\Magento\SalesRule\Model\Rule');
    $model = $salesRule
    ->getCollection()
    ->addFieldToFilter('name',array('eq'=> sprintf('Hive Discount - %s', $couponCode)))
    ->getFirstItem();

    $model->delete();
  }

  /**
   * Get the hive discount applied
   * @param  [type] $rulesIds   [description]
   * @param  [type] $couponCode [description]
   * @return [type]             [description]
   */
  private function getHiveDiscount($rulesIds, $couponCode) {
    $cartRuleId = null;

    if (is_array($rulesIds)) {
      $cartRuleId = $rulesIds[0];
    } else {
      $cartRuleId = $rulesIds;
    }
    // get SalesRule data
    $model = $this->salesRule
    ->getCollection()
    ->addFieldToFilter('name',array('eq'=> sprintf('Hive Discount - %s', $couponCode)))
    ->getFirstItem();

    // check if is the same id
    if ($model && $cartRuleId && $model->getId() == $cartRuleId) {
      // return discount info
      return $model->getData('discount_amount');
    } else {
      return 0;
    }
  }

  /**
   * Get the current brand url
   * @param  [type] $url [description]
   * @return [type]      [description]
   */
  private function getBrandUrl() {
    $url = $this->storeManager->getStore()->getBaseUrl();
    $brandUrl = str_replace('https://', '', $url);
    $brandUrl = str_replace('http://', '', $url);
    // remove the end /
    if (substr($brandUrl, -1) == '/') {
      $brandUrl = substr($brandUrl, 0, (strlen($brandUrl)-1));
    }
    return $brandUrl;
  }
}
